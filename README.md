Yii2  Flat Theme 
======================
Theme for Yii2 Web Applicaiton

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kongoon/yii2-theme-flat "*"
```

or add

```
"kongoon/yii2-theme-flat": "*"
```

to the require section of your `composer.json` file.


Usage
-----
```
use kongoon\theme\flat;

flat\FlatAsset::register($this);
```